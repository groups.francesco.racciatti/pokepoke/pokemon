FROM python:latest

# Install gRPC dependencies
RUN python -m pip install --upgrade pip
RUN python -m pip install grpcio
RUN python -m pip install grpcio-tools

# Gets the proto file
RUN git clone https://gitlab.com/groups.francesco.racciatti/pokepoke/pokemon.git

# Compile the proto file
WORKDIR pokemon
RUN python -m grpc_tools.protoc --proto_path=src pokemon.proto --python_out=src/.